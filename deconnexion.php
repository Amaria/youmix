
<?php
session_start();

    include_once ('ClassDB.php');
    include_once ('CONFIG.php');

// Suppression des variables de session et de la session
$_SESSION = array();
session_destroy();

// Suppression des cookies de connexion automatique
setcookie('login', '');
setcookie('pass_hache', '');

header('refresh:0.5;URL=inscription.php');

?>
       
