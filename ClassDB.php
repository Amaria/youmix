
<?php


// echo $Text;
// echo $Id;

include_once ('CONFIG.php');

class DB {

    static function add($Text){
        $bdd = connect();
        $request = $bdd->prepare("INSERT INTO Video VALUES(NULL, :Text)");
        $request->execute(['Text'=>$Text]);
       
    }

    static function delete($Id){
        $bdd = connect();
        $request = $bdd->prepare('DELETE FROM Video WHERE Id=:Id');
        $request->execute(['Id'=>$Id]);
    }
    
// ajout done en INT
    static function update($Id,$Text){
        $bdd = connect();
        $request = $bdd->prepare('UPDATE Video SET Text=:Text WHERE Id=:Id');
        $request->execute(['Text'=>$Text,'Id'=>$Id]);
    }

    static function inscription($pseudo,$mail,$pass_hash){
        $bdd = connect();
        $done = $bdd->prepare('INSERT INTO Utilisateurs VALUES(Null, :pseudo, :mail, :pass)');
        $done->execute(array(
                'pseudo'=>$pseudo,
                'mail'=>$mail,
                'pass'=>$pass_hash
            ));
    }

    static function connexion($pseudo,$pass_hash){
        $bdd = connect();
        $req = $bdd->prepare('SELECT id FROM Utilisateurs WHERE pseudo = :pseudo AND pass = :pass');
        $req->execute(array(
            'pseudo' => $pseudo,
            'pass' => $pass_hash));
        
        $resultat = $req->fetchAll();
        
        if (!$resultat){
            echo 'Mauvais identifiant ou mot de passe !';
        }
        else{
            session_start();
            $_SESSION['id'] = $resultat[0]['id'];
            $_SESSION['pseudo'] = $pseudo;
            echo '<h3>'.'Vous vous connectez à votre playlist!'.'</h3>';
            header('refresh:2;URL=../Index.php');
        }

        //         if (isset($_SESSION['id']) AND isset($_SESSION['pseudo']))
        // {
        //     echo 'Bonjour ' . $_SESSION['pseudo'];
        // }

}
}



?>